# Patroni

This compose uses autoscaling, which can be done with `scale` option:

```
docker-compose build .
docker-compose up 
docker-compose ps
```

Check:

```
docker-compose exec pg1 patronictl list
```

Clean up:

```
docker-compose down && sudo rm -rf .data
```

Login into root can be:

```
docker exec -u 0 -it patroni_w_envoy_pg1_1 bash
```

```
curl  http://localhost:8008 | jq .
```

UI:

http://192.168.0.176:17000/



## Details

Envoy integration inspired in:
https://github.com/ongres/envoy-postgres-stats-example

UI available http://192.168.0.176:18001/

Prometheus 19090
Grafana 

RW:
psql -h localhost -p 15432 -Upostgres

RO:
psql -h localhost -p 15444 -Upostgres


Get container id to know which is the hostname in the patronictl list:

```
docker ps | grep pg1
```

## PgBouncer

```
PGPASSWORD=postgres psql -h localhost -p 16432 -Upostgres postgres
```