#!/bin/bash

set -e
PGPORT="$1"
PGDATA=/var/lib/postgresql/data-$PGPORT
shift
master_host="$1"
shift
backup_host="$1"
shift
postgres_opts=("$@")

echo "Starting delayed postgres standby instance on port $PGPORT"

rm -Rf $PGDATA || true
mkdir -p $PGDATA
chown postgres:postgres $PGDATA
chmod 700 $PGDATA
echo "Waiting for backup of $PGPORT..."
while ! nc ${backup_host} 15432 2>/dev/null | tar xC $PGDATA 2>/dev/null
do
  sleep 0.3
done

echo "Enabling delayed recovery standby"
echo "
standby_mode = on
recovery_target_timeline='latest'
primary_conninfo = 'host=${master_host} port=$PGPORT'
recovery_min_apply_delay = 20s" > $PGDATA/recovery.conf
chown postgres:postgres $PGDATA/recovery.conf

if [ ! -f "$PGDATA/recovery.conf" ]
then
  mv "$PGDATA/recovery.done" "$PGDATA/recovery.conf"
fi

chmod a+w /var/log

nohup sh > /dev/null 2>&1 << EOF &
  exec su postgres -c "pg_ctl start -D $PGDATA"
EOF

while [ ! -f /var/log/postgres-$PGPORT.log ]; do sleep 1; done
tail -f /var/log/postgres-$PGPORT.log &
echo $! > /var/run/tail-$PGPORT.pid

while ! psql -U postgres -p $PGPORT -d template1 -c "SELECT 1" 1>/dev/null 2>&1; do sleep 0.3; done;
echo "All delayed postgres standby are ready!";

kill $(cat /var/run/tail-$PGPORT.pid) 2>/dev/null || true
