#!/bin/bash

set -e

PGPORT=$1
shift

PGDATA=/var/lib/postgresql/data-$PGPORT
echo "Stopping postgres instance on port $PGPORT"
  
su - postgres -c "pg_ctl -D $PGDATA promote"
