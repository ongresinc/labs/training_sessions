## This Dockerfile is meant to aid in the building and debugging patroni whilst developing on your local machine
## It has all the necessary components to play/debug with a single node appliance, running etcd
FROM postgres:12
MAINTAINER Emanuel Calvo  <emanuel@ongres.com>

RUN export DEBIAN_FRONTEND=noninteractive \
    && echo 'APT::Install-Recommends "0";\nAPT::Install-Suggests "0";' > /etc/apt/apt.conf.d/01norecommend \
    && apt-get update -y \
    && apt-get upgrade -y \
    && apt-get install -y apt-utils procps curl jq dnsutils

RUN apt-get install -y python3-pip python3-setuptools python3-distutils python3-psutil locales  \
    # && apt-get install -y  libpq-dev git curl jq python3-pip python3-setuptools python3-prettytable locales \
    ## Make sure we have a en_US.UTF-8 locale available
    && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
RUN pip3 install --upgrade pip

RUN pip3 install psycopg2-binary patroni[consul] click tzlocal cdiff pysocks \
    && mkdir -p /home/postgres \
    && chown postgres:postgres /home/postgres \
    # Clean up
    && apt-get remove -y python3-pip python3-setuptools \
    && apt-get purge -y --auto-remove \
    && rm -rf /var/lib/apt/lists/* /root/.cache

# ENV CONFDVERSION 0.11.0
# RUN curl -L https://github.com/kelseyhightower/confd/releases/download/v${CONFDVERSION}/confd-${CONFDVERSION}-linux-amd64 > /usr/local/bin/confd \
#     && chmod +x /usr/local/bin/confd

ADD entrypoint.sh /
# # ADD patroni /patroni/
# RUN cd / && git clone https://github.com/zalando/patroni.git patroni_src \
#     && cp -R patroni_src/patroni /patroni \
#     && ls /patroni \
#     && cp /patroni_src/patronictl.py  / \
#     && cp /patroni_src/patroni.py  / \
#     && cp -R /patroni_src/extras/confd /etc/confd \
#     && chmod +x /patroni*py
# RUN ln -s /patronictl.py /usr/local/bin/patronictl
# RUN ln -s /patroni.py /usr/local/bin/patroni


RUN mkdir /data/ &&  touch /pgpass && touch /patroni.yml \
    # && chown postgres:postgres -R /patroni/ /patroni.py /patronictl.py \
    #     /data/ /pgpass /var/run/ /var/lib/ /var/log/
    && chown postgres:postgres -R /patroni.yml /data /tmp /pgpass /var/run /var/lib /var/log
#     && chown postgres:postgres /patroni*py && chmod +x /patroni*py

EXPOSE 2379 5432 8008
VOLUME /data

ENV LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
ENTRYPOINT ["/bin/bash", "/entrypoint.sh"]
USER postgres
