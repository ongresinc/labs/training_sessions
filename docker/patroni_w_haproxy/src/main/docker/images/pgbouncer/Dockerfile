# vim:set ft=dockerfile:
#
# 
#

# FROM debian:buster-slim                        
FROM ubuntu:focal
# Following https://github.com/docker-library/postgres path
MAINTAINER Emanuel Calvo <emanuel@ongres.com> 

# In prod we may want to default to postgres as an extra security layer (capped users list)
ENV PGBPORT 6432
ENV PGBUSER pgbouncer
ENV PGBVERSION 1.15.0
ENV PGBURL http://www.pgbouncer.org/downloads/files/${PGBVERSION}/pgbouncer-${PGBVERSION}.tar.gz
# ENV PGBURL http://pgbouncer.github.io/downloads/files/${PGBVERSION}/pgbouncer-${PGBVERSION}.tar.gz
ENV PGBSHAURL ${PGBURL}.sha256

EXPOSE          $PGBPORT

# Dependencies
RUN             apt-get update && apt-get upgrade -y
RUN             apt-get install -y --no-install-recommends locales curl \
                    gnupg dirmngr build-essential libevent-dev libssl-dev libc-ares-dev pkg-config && \
                apt-get purge -y --auto-remove && rm -rf /var/lib/apt/lists/*

# explicitly set user/group IDs
# https://salsa.debian.org/postgresql/postgresql-common/blob/997d842ee744687d99a2b2d95c1083a2615c79e8/debian/postgresql-common.postinst#L32-35
# https://github.com/docker-library/postgres/issues/274

RUN set -eux; \
	groupadd -r $PGBUSER --gid=999; \
	useradd -r -g $PGBUSER --uid=999 --home-dir=/var/lib/$PGBUSER --shell=/bin/bash $PGBUSER; \
	mkdir -p /var/lib/$PGBUSER; \
	chown -R $PGBUSER:$PGBUSER /var/lib/$PGBUSER

# make the "en_US.UTF-8" locale so $PGBUSER will be utf-8 enabled by default
# if this file exists, we're likely in "debian:xxx-slim", and locales are thus 
# being excluded so we need to remove that exclusion (since we need locales)
RUN set -eux; \
	if [ -f /etc/dpkg/dpkg.cfg.d/docker ]; then \
		grep -q '/usr/share/locale' /etc/dpkg/dpkg.cfg.d/docker; \
		sed -ri '/\/usr\/share\/locale/d' /etc/dpkg/dpkg.cfg.d/docker; \
		! grep -q '/usr/share/locale' /etc/dpkg/dpkg.cfg.d/docker; \
	fi; \
	localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG en_US.utf8


RUN             curl -So pgbouncer-${PGBVERSION}.tar.gz ${PGBURL} \
                && curl -So pgbouncer-${PGBVERSION}.tar.gz.sha256 ${PGBSHAURL}

RUN             cat pgbouncer-${PGBVERSION}.tar.gz.sha256 | sha256sum -c -

RUN             tar -zxf pgbouncer-${PGBVERSION}.tar.gz \
                && chown root:root pgbouncer-${PGBVERSION}

RUN             cd pgbouncer-${PGBVERSION} \
                && ./configure --prefix=/usr/local \
                    --with-libevent=libevent-prefix \
                    --with-cares=cares-prefix \
                    --with-openssl=openssl-prefix \
                && make && make install && make clean

ADD             conf /etc/pgbouncer
VOLUME          /etc/pgbouncer

# Make sure pgbouncer user can read and write log files
RUN             mkdir -p /var/log/$PGBUSER && chown -R $PGBUSER:$PGBUSER /var/log/$PGBUSER
RUN             mkdir /var/run/$PGBUSER && chown -R $PGBUSER:$PGBUSER /var/run/$PGBUSER  

USER            $PGBUSER
ENTRYPOINT      ["pgbouncer"]
CMD             ["/etc/pgbouncer/pgbouncer.ini"]

