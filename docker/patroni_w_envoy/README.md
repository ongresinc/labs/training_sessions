# Patroni

This compose uses autoscaling, which can be done with `scale` option:

```
docker-compose build .
docker-compose up -d
docker-compose ps
```

Login into root can be:

```
docker exec -u 0 -it patroni_w_envoy_pg1_1 bash
```

```
curl  http://localhost:8008 | jq .
```


## Details

Envoy integration inspired in:
https://github.com/ongres/envoy-postgres-stats-example

UI available http://192.168.0.176:18001/

Prometheus 19090
Grafana 13080

Get status of nodes:

```
http://192.168.0.176:18001/clusters
```


```
psql -h localhost -p 1999 -Upostgres
psql (12.6 (Ubuntu 12.6-0ubuntu0.20.04.1))
Type "help" for help.

postgres=# 
```

> NOTE: The database has traffic by default through traffic container


```
# If you want to increase or decrease scale:
pgbench -h localhost -p 1999 -Upostgres -i -s 30
pgbench -h localhost -p 1999 -Upostgres -c 20 -C -T 120
```

Testing pgbouncer with DNS integration:

```
pgbench -h localhost -p 16432 -Upostgres -c 10
```

```
docker-compose exec pg1 patronictl list
docker-compose exec pg1 patronictl failover
```

# Envoy

Patroni API states:

```
The following requests to Patroni REST API will return HTTP status code 200 only when the Patroni node is running as the leader:

GET /
GET /master
GET /leader
GET /primary
GET /read-write
```

```yaml
  clusters:
  - name: postgres_cluster
    connect_timeout: 1s
    type: strict_dns
    load_assignment:
      cluster_name: postgres_cluster
      endpoints:
      - lb_endpoints:
        - endpoint:
            health_check_config:
              port_value: 8008
            address:
              socket_address:
                address: pg1
                port_value: 5432
        - endpoint:
            health_check_config:
              port_value: 8008
            address:
              socket_address:
                address: pg2
                port_value: 5432
    health_checks:
      - timeout: 1s
        interval: 10s
        interval_jitter: 1s
        unhealthy_threshold: 6
        healthy_threshold: 1
        http_health_check:
          path: "/master"
```

