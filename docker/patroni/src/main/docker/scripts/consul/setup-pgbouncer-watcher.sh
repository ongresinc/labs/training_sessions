#!/bin/bash

PGBPORT="$1"
shift
PGPORT="$1"
shift

> /var/log/pgbouncer-update-master.log

script="/tmp/pgbouncer-update-master-$PGBPORT.sh"
echo "bash /scripts/pgbouncer/pgbouncer-update-master.sh $PGBPORT $PGPORT" > "$script"
chmod a+x "$script"

echo '{"watches":[{"type":"service","service":"postgresql","handler":"'"$script"'"}]}' > /var/lib/consul/config/pgbouncer-watcher.json

[ ! -f /var/run/consul.pid ] || kill -1 $(cat /var/run/consul.pid) 2>/dev/null