#!/bin/bash

set -e

PGPORT="$1"
shift

data=$(cat)

date
echo "Watch output was:"
echo "$data"|jq .
echo

for index in echo $(seq 0 "$(echo "$data""|jq 'length-1'))
do
  node="$(echo "$data"|jq '.['"$index"']'.Key|tr '/' '\n'|tail -n 1)"
  
  if [ -z "$node" ]
  then
    continue
  fi
  
  echo "Removing node $node"
  
  if [ "$REPMGR_VERSION" == "3.3" ]
  then
    /usr/local/pgsql/bin/psql -U postgres -d repmgr -c 'DELETE FROM repmgr_main.repl_nodes WHERE node_id = '"$node" \
      || curl -s "http://localhost:8500/v1/kv/repmgrpoc/$PGPORT/failed_masters/$NODE" -X PUT
  else
    /usr/local/pgsql/bin/psql -U postgres -d repmgr -c 'DELETE FROM repmgr.nodes WHERE node_id = '"$node" \
      || curl -s "http://localhost:8500/v1/kv/repmgrpoc/$PGPORT/failed_masters/$NODE" -X PUT
  fi
done