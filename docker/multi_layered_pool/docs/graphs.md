

## Local Pool 

By using REST API Load Balancer's feature (most of them have this feature included), it is possible to 
automatically switch the RW target after a failover or switchover. 

Patroni provides the API for exposing the node state (Leader, Replica) and allows this to be integrated
smoothly.


```mermaid
graph TB
  appPool --> LB
  LB --> localPool
  LB -.->|RO| localRPool
  LB -.- PatroniAPILeader
  LB -.- PatroniAPIReplica

  subgraph AppZone
    App --> appPool["App Pool"]
    appPool
  end

  subgraph LBComponent
    LB["Load Balancer"]
  end

  subgraph Leader
    LeaderNode
    PatroniAPILeader
    localPool --> LeaderNode
  end

  subgraph Replica
    ReplicaNode
    PatroniAPIReplica
    localRPool --> ReplicaNode
  end
```

## DNS integration

Also, Consul provides DNS capabilities, which can expose FQDN poiting to the corresponding nodes
by using the same Patroni's DCS keys, granting consistent procedures all over the components that
depend on the Cluster properties.

```mermaid
graph TB
  appPool --> |leader.clusterName.patroni| LeaderNode
  ConsulServer -.-> ConsulAgentL
  ConsulServer -.-> ConsulAgentR

  subgraph AppZone
    App --> appPool["App Pool"]
    appPool
  end

  subgraph ConsulCluster
    ConsulServer["Consul Server"]
  end

  subgraph Leader
    LeaderNode
    ConsulAgentL
    localPool --> LeaderNode
  end

  subgraph Replica
    ReplicaNode
    ConsulAgentR
    localRPool --> ReplicaNode
  end
```

## Middleware

More complex scenarios may need to be scaled for a greater isolation and workload scope. The bellow graph
shows different layers of pool for different application-families.

```mermaid
graph TB
        QMS -->|Cluster Endpoint| QMSPool
        App -->|Cluster Endpoint| AppPool
        QMSPool --> LBQMS
        AppPool --> LBApp

    subgraph Cluster["Database Network"]
        LBQMS -.->| 150 Client Conn | pgb1Q
        LBQMS -.->| 150 Client Conn | pgb2Q
        LBQMS -.->| 150 Client Conn | pgb3Q
        LBApp -.->| 100 Client Conn | pgb1
        LBApp -.->| 100 Client Conn | pgb2
        LBApp -.->| 100 Client Conn | pgb3
        pgb3Q -->| 4 conn | poolLE
        pgb2Q -->| 4 conn | poolLE
        pgb1Q -->| 4 conn | poolLE
        pgb3 --> | 6 conn | poolLE
        pgb2 --> | 6 conn | poolLE
        pgb1 --> | 6 conn | poolLE
    subgraph QPOOL["Queue Pool Fleet"]
        pgb1Q["PgBouncer1Q"]
        pgb2Q["PgBouncer2Q"]
        pgb3Q["PgBouncer3Q"]
    end
    subgraph APPPOOL["App Pool Fleet"]
        pgb1["PgBouncer1"]
        pgb2["PgBouncer2"]
        pgb3["PgBouncer3"]
    end
    subgraph PG["Patroni Cluster"]
        subgraph Leader["Leader"]
            leader 
            poolLE["Local Pools"] -.-> |Server-conn < max_connections | leader
        end
        subgraph ReplicaN["ReplicaN"]
            replicaN
            poolRN["RE Sidecar Pool"] --> replicaN
        end
    end

    end
```

## For Database-to-Database connection persistance

One of the edge scenarios, is to setup an intra-database pool. Although, the objective here won't be to pool connections but to persist connections, allowing connection establishment latency reduction.

```mermaid

graph TB
        poolFDW --> extDS["External Data Source"]
        poolFDW --> |Low but persistent server connections| poolOP
        
        subgraph Leader["Leader"]
            leader --> poolFDW["FDW  Pool"]
        end

        subgraph OtherPostgresCluster["Other Postgres"]
            OtherPostgres
            poolOP["Local Pool"] --> OtherPostgres
        end
```






