# Patroni

This compose uses autoscaling, which can be done with `scale` option:

```
docker-compose build .
docker-compose up --scale pg=3 -d
```

There are 2 files for Patroni configuration, `patroni.yml` is the auto-generated
configuration. The `patroni-prod.yml` is an extended version, for use it or change
back to default, comment/uncomment volumes at the `pg` service:

```
    volumes:
      - ./patroni-prod.yml:/patroni.yml
```
