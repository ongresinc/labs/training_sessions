# monitoring

1. To start:

```
docker-compose up -d
```

If accessing from toninas from Emanuel's house:

```
http://192.168.0.176:13080/login
```

From outside:

```
http://toninas.mooo.com:13080/login
```

Or through your local browser:

```
http://localhost:3080/login
```


2. default user pass:

```
user: admin
pass: admin
```

Dashboards used (now are configured automatically):

https://grafana.com/grafana/dashboards/9628

PgBouncer dashboard uses its own Datasource (https://grafana.com/grafana/dashboards/9760), it won't work
   with the import. Download `postgresql-pgbouncer_rev3.json` which contains the fixed datasource.



## References:

https://raw.githubusercontent.com/prometheus-community/postgres_exporter/master/queries.yaml

See [dockprom](https://github.com/stefanprodan/dockprom) for other configurations for implementing more advanced monitoring.