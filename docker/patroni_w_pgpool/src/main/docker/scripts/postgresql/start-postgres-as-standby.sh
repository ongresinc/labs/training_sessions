#!/bin/bash

set -e

PGPORT="$1"
PGDATA=/var/lib/postgresql/data-$PGPORT
shift
master_host="$1"
shift
backup_host="$1"
shift

echo "Starting postgres standby instance on port $PGPORT"

useradd postgres || true
rm -Rf $PGDATA || true
mkdir -p $PGDATA
chown postgres:postgres $PGDATA
chmod 700 $PGDATA
echo "Waiting for backup of $PGPORT..."
while ! nc ${backup_host} 1$PGPORT 2>/dev/null | tar xC $PGDATA 2>/dev/null
do
  sleep 0.3
done

echo "Enabling recovery"
echo "
recovery_target_timeline='latest'
standby_mode = on
primary_conninfo = 'host=${master_host} port=$PGPORT'" > $PGDATA/recovery.conf
chown postgres:postgres $PGDATA/recovery.conf

chmod a+w /var/log

nohup sh > /dev/null 2>&1 << EOF &
  exec su postgres -c "pg_ctl start -D $PGDATA"
EOF

while [ ! -f /var/log/postgres-$PGPORT.log ]; do sleep 1; done

tail -f /var/log/postgres-*.log &
echo $! > /var/run/tail.pid

while ! psql -U postgres -p $PGPORT -d template1 -c "SELECT 1" 1>/dev/null 2>&1; do sleep 0.3; done;
echo "All postgres standby are ready!";

kill $(cat /var/run/tail.pid)