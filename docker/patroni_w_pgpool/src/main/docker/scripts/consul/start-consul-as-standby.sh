#!/bin/bash

set -e

nohup sh > /var/log/consul.log 2>&1 << EOF &
  echo "Starting consul master instance"
  mkdir -p /var/lib/consul/data || true
  mkdir -p /var/lib/consul/config || true
  rm -Rf /var/lib/consul/data/* || true
  rm -Rf /var/lib/consul/config/* || true
  
  exec consul agent -data-dir=/var/lib/consul/data -config-dir=/var/lib/consul/config \
    -server -client=0.0.0.0 -bind=0.0.0.0 $@
EOF

echo $! > /var/run/consul.pid

while [ ! -f /var/log/consul.log ]; do sleep 1; done

tail -f /var/log/consul.log &
echo $! > /var/run/tail.pid

while ! consul info|grep -q "state = Follower" 1>/dev/null 2>&1; do sleep 2; done;
echo "Consul standby is ready!";

kill $(cat /var/run/tail.pid)