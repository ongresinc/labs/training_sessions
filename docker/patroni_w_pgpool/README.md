# Patroni

This compose uses autoscaling, which can be done with `scale` option:

```
docker-compose build .
docker-compose up -d
docker-compose ps
```

Login into root can be:

```
docker exec -u 0 -it patroni_w_pgpool_pg1_1 bash
```

```
curl  http://localhost:8008 | jq .
```

PGPASSWORD=postgres psql -h localhost -p 15432 -Upostgres

https://github.com/bitnami/bitnami-docker-pgpool