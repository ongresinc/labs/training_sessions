#!/bin/bash

SCRIPT_PATH=$(dirname `which $0`)
source $SCRIPT_PATH/pgsecrets.env

###
export DEMO_DB="demo"
export CREATE_DB="CREATE DATABASE ${DEMO_DB}"
export DROP_DB="DROP DATABASE ${DEMO_DB} ;"

export SOURCE_TABLE='''
CREATE TABLE IF NOT EXISTS cross_version ( 
    i bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    f text default random()::text
);
'''

export DEST_TABLE='''
CREATE TABLE IF NOT EXISTS cross_version (
    i bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
    f text default random()::text,
    e text
);
'''

export COUNT="select count(*) from cross_version;"
export INZ="insert into cross_version default values;"
export CHECK_VALUES="SELECT i,f FROM cross_version"

function execute_in_all_postgresdb {
for pp in  5 6
do
# echo $pp --
PGPASSWORD=${POSTGRES_PASSWORD} psql -q -h localhost -p 1555${pp} -U postgres  <<EOF
${@}
EOF
done
}

function execute_in_all {
for pp in  5 6
do
# echo $pp --
PGPASSWORD=${POSTGRES_PASSWORD} psql -q -h localhost -p 1555${pp} -U postgres ${DEMO_DB} <<EOF
${@}
EOF
done
}

function execute {
case $1 in
    pg1)
        PGPORT=15555
    ;;
    pg2)
        PGPORT=15556
    ;;
    *)
        echo Non existing server
        # exit
    ;;
esac
shift

PGPASSWORD=${POSTGRES_PASSWORD} psql -q -h localhost -p ${PGPORT} -U postgres ${DEMO_DB} <<EOF
${@}
EOF

}

execute_in_all_postgresdb $CREATE_DB
execute_in_all $EXTENSIONS

execute pg1 $SOURCE_TABLE
execute pg2 $DEST_TABLE

PUBLICATION="CREATE PUBLICATION cross_version_pg1_fulllr FOR ALL TABLES;"
execute pg1 $PUBLICATION

# Insert some data
execute pg1 $INZ
execute pg1 $COUNT
execute pg1 $CHECK_VALUES

SUBSCRIPTION="
CREATE SUBSCRIPTION cross_version_pg2_fulllr \
  CONNECTION 'host=pg1 user=postgres password=postgres dbname=demo' PUBLICATION cross_version_pg1_fulllr \
  WITH (copy_data =  true, enabled = true);
"

execute pg2 $SUBSCRIPTION
sleep 5
execute pg2 $COUNT
execute pg2 $CHECK_VALUES

for i in seq 1 .. 10; do
    execute pg1 $INZ
done
sleep 5
echo "Checking that records have been replicated:"
execute pg2 $COUNT
execute pg2 $CHECK_VALUES

read -p "Continue? " -n 1 -r


DROP_PUB="DROP PUBLICATION cross_version_pg1_fulllr;"
execute pg1 $DROP_PUB
DROP_SUBS="DROP SUBSCRIPTION cross_version_pg2_fulllr;"
execute pg2 $DROP_SUBS

execute_in_all_postgresdb $DROP_DB
