# Docker Lab SQL optimizations in PG 12


To init the lab execute `sh init.sh`, this file will create a PostgreSQL server in a container, changes some PostgreSQL's configuration, load a sample database and run some queries:

```
sh init.sh

```
_Note: this could take some time and should setup all the necessary pieces for running the lab, please be patient_

To remove the lab execute:

```
sh clean.sh
```

To connect to PostgreSQL's server:

```
docker-compose exec lab_postgres12 /bin/bash -c "psql -U postgres -d dell"
```


To connect container:

```
docker-compose exec lab_postgres12 /bin/bash 
```

