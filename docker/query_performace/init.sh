#!/bin/bash
set -e


[ -z $(docker-compose ps -q ) ] && docker-compose up -d


docker-compose exec lab_postgres12 /bin/bash -c  "apt-get update && apt-get install -y pgbadger" 
        

cat > ./pg_data/postgresql.conf << EOF        
 shared_preload_libraries='pg_stat_statements'
 log_min_duration_statement='50' 
 logging_collector = 'on'
 log_connections = 'on'
 log_autovacuum_min_duration = '0'
EOF



docker-compose restart lab_postgres12


until docker-compose exec lab_postgres12 /bin/bash -c "pg_isready -Upostgres -d dell" ; do sleep 1; done

docker-compose exec lab_postgres12 /bin/bash -c  "psql -U postgres -d dell -f /scripts/dellstore2.sql"
docker-compose exec lab_postgres12 /bin/bash -c  "psql -U postgres -d dell -c 'create extension pg_stat_statements;create extension pg_trgm;'"
docker-compose exec lab_postgres12 /bin/bash -c  "psql -U postgres -d dell -f /scripts/texts.sql"
docker-compose exec lab_postgres12 /bin/bash -c  "pgbench -n -c 20 -T 30 -d -f /scripts/custom_queries.sql  -U postgres dell"





