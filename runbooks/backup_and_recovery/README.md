
Backups runbooks are based in the `vagrant/pgbackrest` environment.

```
cd vagrant/pgbackrest
make up
make provision
```

Let's generate garbage data in the DB before executing any dumps:

```
createdb demo
/usr/pgsql-12/bin/pgbench -i demo
```

On backup server, install clients

```
sudo yum install -y postgresql12
```