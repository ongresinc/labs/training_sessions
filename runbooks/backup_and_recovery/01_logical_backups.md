
```
vagrant ssh db-server-1
sudo su - postgres
```

`pg_dumpall`:

```
/usr/pgsql-12/bin/pg_dumpall -r -f roles.sql
head roles.sql
```

`pg_dump`:

```
time pg_dump -f demo.sql demo
time pg_dump -j 2  -f demo.sql demo
```

A restore test:

```
createdb demo2
psql demo2 < demo.sql
```

Testing with clean:

```
# should give an error
psql demo2 < demo.sql

# Added clean
time pg_dump -j 2 -c -f demo.sql demo
psql demo2 < demo.sql
```


```
pg_dump -F c -c -f demo.custom demo
createdb demo
pg_restore -C -c -d demo demo.custom
psql demo
```


```
pg_dump -C pagila  | sed 's/CREATE DATABASE pagila/CREATE DATABASE pagila_restore/g' | sed 's/connect pagila/connect pagila_restore/g' | psql
```
