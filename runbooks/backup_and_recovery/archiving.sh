## Check

show archive_mode;


## CREATE the "remote folder"
sudo mkdir /mnt/remote
sudo chown postgres.postgres /mnt/remote

## turn on archiving


ALTER SYSTEM SET archive_mode = 'on';

## For easy understanding, open the conf file and locate archive_command and explain %p and %f. Then implement archiving with
SHOW config_file;

archive_command = 'cp %p /mnt/remote/%f';

## Set a short timeout 
ALTER SYSTEM SET archive_timeout = '1min';

## Restart the service and check

pg_ctlcluster 12 main restart

show archive_mode;
show archive_command;

-- AFTER a while, check /mnt/remote and pg_stat_archiver view
