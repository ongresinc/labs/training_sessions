
Manual:

```
psql -c "select pg_start_backup('demo');"
 pg_start_backup 
-----------------
 0/9000028
(1 row)

# Copy the data directory

psql -c "select pg_stop_backup();"
NOTICE:  all required WAL segments have been archived
 pg_stop_backup 
----------------
 0/9000138
(1 row)
```


pg_basebackup from backup server:

```
PGPASSWORD=postgres pg_basebackup -h 10.0.0.12 -Upostgres -D /tmp/test --checkpoint=fast -P -R
```