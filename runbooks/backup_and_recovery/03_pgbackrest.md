# PgBackRest Demo

```
make up provision
```

Full information of the setup:
[Centos User Guide](https://pgbackrest.org/user-guide-centos8.html)

[Command Reference](https://pgbackrest.org/command.html)

```
sudo su - postres
```

## Details

```
db-server-1               running (virtualbox)
db-server-2               running (virtualbox)
backup                    running (virtualbox)
```

The setup is under `roles/pgbackrest/tasks/server.yml`

```
vagrant ssh backup
```

```
pgbackrest_backup_directory: /var/lib/pgbackrest
pgbackrest_log_directory: /var/log/pgbackrest
```

User in backup server is pgbackrest:

```
[vagrant@backup ~]$ sudo crontab -l -u pgbackrest
#Ansible: backup for the 'db-server-1' stanza
0 22 * * * pgbackrest --stanza=db-server-1 backup
#Ansible: backup for the 'db-server-2' stanza
0 22 * * * pgbackrest --stanza=db-server-2 backup
```

User in db nodes is postgres.


```
-bash-4.2$ pgbackrest info
stanza: db-server-1
    status: ok
    cipher: none

    db (current)
        wal archive min/max (12): 000000010000000000000001/000000010000000000000002

        full backup: 20210517-212705F
            timestamp start/stop: 2021-05-17 21:27:05 / 2021-05-17 21:27:09
            wal start/stop: 000000010000000000000002 / 000000010000000000000002
            database size: 23.4MB, database backup size: 23.4MB
            repo1: backup set size: 2.8MB, backup size: 2.8MB

stanza: db-server-2
    status: ok
    cipher: none

    db (current)
        wal archive min/max (12): 000000010000000000000001/000000010000000000000002

        full backup: 20210517-212717F
            timestamp start/stop: 2021-05-17 21:27:17 / 2021-05-17 21:27:22
            wal start/stop: 000000010000000000000002 / 000000010000000000000002
            database size: 23.4MB, database backup size: 23.4MB
            repo1: backup set size: 2.8MB, backup size: 2.8MB
```

Base Backups:

```
[pgbackrest@backup pgbackrest]$ ls -l /var/lib/pgbackrest/backup/db-server-1/
total 8
drwxr-x---. 3 pgbackrest pgbackrest   72 May 17 21:27 20210517-212705F
drwxr-x---. 3 pgbackrest pgbackrest   72 May 17 22:00 20210517-212705F_20210517-220002I
drwxr-x---. 3 pgbackrest pgbackrest   18 May 17 21:27 backup.history
-rw-r-----. 1 pgbackrest pgbackrest 1633 May 17 22:00 backup.info
-rw-r-----. 1 pgbackrest pgbackrest 1633 May 17 22:00 backup.info.copy
lrwxrwxrwx. 1 pgbackrest pgbackrest   33 May 17 22:00 latest -> 20210517-212705F_20210517-220002I
```

Archives:

```
[pgbackrest@backup pgbackrest]$ ls -l /var/lib/pgbackrest/archive/db-server-1/12-1/0000000100000000/00000001000000000000000*
-rw-r-----. 1 pgbackrest pgbackrest 1622446 May 17 21:27 /var/lib/pgbackrest/archive/db-server-1/12-1/0000000100000000/000000010000000000000001-3b934f0b06634c1cd5004a7bb33ca229ca476bbc.gz
-rw-r-----. 1 pgbackrest pgbackrest     370 May 17 21:27 /var/lib/pgbackrest/archive/db-server-1/12-1/0000000100000000/000000010000000000000002.00000028.backup
-rw-r-----. 1 pgbackrest pgbackrest   16486 May 17 21:27 /var/lib/pgbackrest/archive/db-server-1/12-1/0000000100000000/000000010000000000000002-5bd59df69d1cfbf6ae08c2d22118f26a4fefed20.gz
-rw-r-----. 1 pgbackrest pgbackrest   16479 May 17 22:00 /var/lib/pgbackrest/archive/db-server-1/12-1/0000000100000000/000000010000000000000003-88a9c6033be9b417a22d6acd725c44a9cd639c66.gz
-rw-r-----. 1 pgbackrest pgbackrest     370 May 17 22:00 /var/lib/pgbackrest/archive/db-server-1/12-1/0000000100000000/000000010000000000000004.00000028.backup
-rw-r-----. 1 pgbackrest pgbackrest   16488 May 17 22:00 /var/lib/pgbackrest/archive/db-server-1/12-1/0000000100000000/000000010000000000000004-a5130678b3d1c769e53da278b47033023593cc90.gz
```

## Restoring in a different directory

Useful for restoring for test purposes:

echo -e "\nport=5555" >> /tmp/datatest/postgresql.auto.conf

```
-bash-4.2$ mkdir /tmp/datatest
-bash-4.2$ pgbackrest --stanza=db-server-1 --pg1-path=/tmp/datatest restore 
-bash-4.2$ echo -e "\nport=5555" >> /tmp/datatest/postgresql.conf
-bash-4.2$ /usr/pgsql-12/bin/pg_ctl -D /tmp/datatest/ start 
-bash-4.2$ ps f -Cpostgres
  PID TTY      STAT   TIME COMMAND
29484 ?        Ss     0:00 /usr/pgsql-12/bin/postgres -D /tmp/datatest
29490 ?        Ss     0:00  \_ postgres: checkpointer   
29491 ?        Rs     0:00  \_ postgres: background writer   
29493 ?        Ss     0:00  \_ postgres: stats collector   
29501 ?        Rs     0:00  \_ postgres: walwriter   
29502 ?        Ss     0:00  \_ postgres: autovacuum launcher   
29503 ?        Ss     0:00  \_ postgres: archiver   failed on 00000002.history
29504 ?        Ss     0:00  \_ postgres: logical replication launcher   
```

To setup as a replica:

> Note: Add the line in the pg_hba.conf!

```
echo -e "host    replication    all       127.0.0.1/32   md5\n" >> /var/lib/pgsql/12/data/pg_hba.conf
```

```
primary_conninfo = 'host=127.0.0.1 port=5432 password=postgres user=postgres sslmode=prefer'
```


