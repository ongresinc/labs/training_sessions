### Adding logging capabilities

### Open the log file, modify
log_connections = on
log_disconnections = on

### Reload pg, show the log and explain

## Add duration
log_min_duration_statement =0
## Reload pg, show the log again


## Activate random sampling (new in 12)
log_min_duration_statement = -1
log_transaction_sample_rate = 0.2

## Modify log_line_prefix and use application_name to track

log_line_prefix = '%m [%p] %q%u@%d (%a)'

## Logging waits
log_lock_waits = on -- (related to deadlock_timeout)


