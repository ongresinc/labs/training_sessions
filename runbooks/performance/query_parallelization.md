# Query parallelization

```
CREATE TABLE bigone (id serial primary key, data text);
insert into bigone (data) select md5(i::text) from generate_series(1,5000000) i;
```


## Degree of paralellism


```
show max_parallel_workers_per_gather;
```

```
explain analyze select count(*) from bigone;
```

```
postgres=# set max_parallel_workers_per_gather TO 2;
postgres=# explain analyze select count(*) from bigone;
                                                                  QUERY PLAN                                                                  
----------------------------------------------------------------------------------------------------------------------------------------------
 Finalize Aggregate  (cost=136416.81..136416.82 rows=1 width=8) (actual time=454.004..456.478 rows=1 loops=1)
   ->  Gather  (cost=136416.60..136416.81 rows=2 width=8) (actual time=453.798..456.467 rows=3 loops=1)
         Workers Planned: 2
         Workers Launched: 2
         ->  Partial Aggregate  (cost=135416.60..135416.61 rows=1 width=8) (actual time=422.946..422.947 rows=1 loops=3)
               ->  Parallel Seq Scan on bigone  (cost=0.00..125000.08 rows=4166608 width=0) (actual time=0.129..270.668 rows=3333333 loops=3)
 Planning Time: 0.254 ms
 Execution Time: 456.591 ms
 ```

## Now add more parallel workers and check again

```
set max_parallel_workers_per_gather TO 4;
```

```
postgres=# explain analyze select count(*) from bigone;
                                                                  QUERY PLAN                                                                  
----------------------------------------------------------------------------------------------------------------------------------------------
 Finalize Aggregate  (cost=115583.97..115583.98 rows=1 width=8) (actual time=379.553..382.156 rows=1 loops=1)
   ->  Gather  (cost=115583.56..115583.96 rows=4 width=8) (actual time=379.437..382.145 rows=5 loops=1)
         Workers Planned: 4
         Workers Launched: 4
         ->  Partial Aggregate  (cost=114583.56..114583.57 rows=1 width=8) (actual time=329.863..329.864 rows=1 loops=5)
               ->  Parallel Seq Scan on bigone  (cost=0.00..108333.65 rows=2499964 width=0) (actual time=0.100..213.950 rows=2000000 loops=5)
 Planning Time: 0.192 ms
 Execution Time: 382.278 ms
(8 rows)
```


If you are in a VM, play around with 3 settings:

- Disabling gather to 0
  - Under using resources
- Setting = number of cores in the VM
  - Best scenario
- Setting the gather double to the VM
  - This will show worser performance than setting =

We show here the impact of having more workers than available CPU resources.