# Basic activity generation with pgbench

```
sudo yum install -y postgresql13-server postgresql13 pg_repack13 postgresql13-contrib
sudo /usr/pgsql-13/bin/postgresql-13-setup initdb
sudo systemctl enable postgresql-13
sudo systemctl start postgresql-13

ps f -Cpostmaster

sudo su - postgres
createdb demo
```


```
# Initialize dataset
pgbench -i -s 400 demo

# generate some activity
pgbench -c 10 -T 60 demo &
```

```
cd /var/lib/pgsql/13/data/

-bash-4.2$ ls -l base/16384/
total 6154048
-rw-------. 1 postgres postgres       8192 May 17 20:56 112
-rw-------. 1 postgres postgres       8192 May 17 20:56 113
-rw-------. 1 postgres postgres      81920 May 17 21:03 1247
-rw-------. 1 postgres postgres      24576 May 17 20:56 1247_fsm
-rw-------. 1 postgres postgres       8192 May 17 21:03 1247_vm
-rw-------. 1 postgres postgres     434176 May 17 21:08 1249
-rw-------. 1 postgres postgres      24576 May 17 21:03 1249_fsm
-rw-------. 1 postgres postgres       8192 May 17 21:03 1249_vm

/usr/pgsql-13/bin/vacuumdb demo

## Example with table segments, FSM and VM.

-rw-------. 1 postgres postgres 1073741824 May 17 21:11 16397
-rw-------. 1 postgres postgres 1073741824 May 17 21:11 16397.1
-rw-------. 1 postgres postgres 1073741824 May 17 21:11 16397.2
-rw-------. 1 postgres postgres 1073741824 May 17 21:11 16397.3
-rw-------. 1 postgres postgres 1073741824 May 17 21:11 16397.4
-rw-------. 1 postgres postgres   13123584 May 17 21:11 16397.5
-rw-------. 1 postgres postgres    1343488 May 17 21:11 16397_fsm
-rw-------. 1 postgres postgres     172032 May 17 21:11 16397_vm

```