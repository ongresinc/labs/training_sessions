
Execute in `vagrant/sandbox`:

```
make up
# If provision fails for some reason:
make provision
```

Centos:
```
sudo yum install -y postgresql13-server postgresql13 pg_repack13 postgresql13-contrib
sudo /usr/pgsql-13/bin/postgresql-13-setup initdb
sudo systemctl enable postgresql-13
sudo systemctl start postgresql-13
```

Ubuntu
```
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'

wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

sudo apt-get update

sudo apt-cache search postgresql-12

sudo apt-get -y install postgresql-12 postgresql-12-repack

sudo -u postgres createdb demo
sudo -u postgres pgbench -i demo -s 30 
psql -c "CREATE EXTENSION pg_repack;" -d demo

sudo systemctl restart postgresql.service

sudo -u postgres pg_repack -d demo
```

```
ps f -Cpostmaster

sudo su - postgres
createdb demo
/usr/pgsql-13/bin/pgbench -i demo
psql -c "CREATE EXTENSION pg_repack;" -d demo

/usr/pgsql-13/bin/psql -c "alter system set shared_preload_libraries=pg_repack;" demo

sudo systemctl restart postgresql-13
```

This repacks all the database!
```
/usr/pgsql-13/bin/pg_repack -d demo
```

