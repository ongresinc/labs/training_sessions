## Terminal 1: Open a transaction and hold some rows
BEGIN;
update some_data set data = 'ZZZ' where data = 'xxx';
-- Let the transaction Open


## Terminal 2: show that update is not visible yet
select * from some_data where data = 'ZZZ';
## And try to update some
update some_data set data = 'ZZZ' where data = 'xxx';

## terminal 3: Show some magic with pg_stat_activity and explain

select * from pg_stat_activity where query ~ 'update';


## Spot more precisly who is blocking who
select pid, query, wait_event, wait_event_type,pg_blocking_pids(pid) from pg_stat_activity where query ~ 'update';
