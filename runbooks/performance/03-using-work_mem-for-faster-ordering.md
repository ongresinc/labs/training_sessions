### Using the table from previous exercise

```
postgres=# show work_mem ;
 work_mem 
----------
 4MB
(1 row)
```


```
postgres=# explain analyze select * from bigone order by data limit 100000;
                                                                   QUERY PLAN                                                                   
------------------------------------------------------------------------------------------------------------------------------------------------
 Limit  (cost=811991.86..823659.34 rows=100000 width=37) (actual time=12458.008..12698.884 rows=100000 loops=1)
   ->  Gather Merge  (cost=811991.86..1784268.27 rows=8333216 width=37) (actual time=12444.186..12678.317 rows=100000 loops=1)
         Workers Planned: 2
         Workers Launched: 2
         ->  Sort  (cost=810991.83..821408.35 rows=4166608 width=37) (actual time=12348.450..12370.129 rows=33892 loops=3)
               Sort Key: data
               Sort Method: external merge  Disk: 153728kB
               Worker 0:  Sort Method: external merge  Disk: 153152kB
               Worker 1:  Sort Method: external merge  Disk: 153152kB
               ->  Parallel Seq Scan on bigone  (cost=0.00..125000.08 rows=4166608 width=37) (actual time=0.039..1306.542 rows=3333333 loops=3)
 Planning Time: 0.217 ms
 JIT:
   Functions: 1
   Options: Inlining true, Optimization true, Expressions true, Deforming true
   Timing: Generation 0.398 ms, Inlining 0.359 ms, Optimization 6.951 ms, Emission 6.258 ms, Total 13.965 ms
 Execution Time: 12741.006 ms
(16 rows)
```

### Check for "external merge" nodes.
### Then add more work_mem and try again.


```
postgres=# set work_mem TO '128MB';
SET
postgres=# explain analyze select * from bigone order by data limit 100000;
                                                                  QUERY PLAN                                                                   
-----------------------------------------------------------------------------------------------------------------------------------------------
 Limit  (cost=492862.44..504529.92 rows=100000 width=37) (actual time=3137.183..3191.090 rows=100000 loops=1)
   ->  Gather Merge  (cost=492862.44..1465138.86 rows=8333216 width=37) (actual time=3066.068..3114.226 rows=100000 loops=1)
         Workers Planned: 2
         Workers Launched: 2
         ->  Sort  (cost=491862.42..502278.94 rows=4166608 width=37) (actual time=3013.918..3019.127 rows=33807 loops=3)
               Sort Key: data
               Sort Method: top-N heapsort  Memory: 19370kB
               Worker 0:  Sort Method: top-N heapsort  Memory: 19364kB
               Worker 1:  Sort Method: top-N heapsort  Memory: 19377kB
               ->  Parallel Seq Scan on bigone  (cost=0.00..125000.08 rows=4166608 width=37) (actual time=0.021..403.468 rows=3333333 loops=3)
 Planning Time: 13.803 ms
 JIT:
   Functions: 1
   Options: Inlining true, Optimization true, Expressions true, Deforming true
   Timing: Generation 13.229 ms, Inlining 1.493 ms, Optimization 38.915 ms, Emission 29.036 ms, Total 82.673 ms
 Execution Time: 3208.720 ms
(16 rows)

```

```
while true ; do psql -c "select * from bigone order by data limit 100000;" demo & sleep 1; done


[ 9732.369928] Out of memory: Kill process 9138 (postgres) score 154 or sacrifice child
[ 9732.370696] Killed process 9138 (postgres) total-vm:401152kB, anon-rss:18524kB, file-rss:137292kB
```