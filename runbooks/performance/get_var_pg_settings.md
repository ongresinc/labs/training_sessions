# pg_settings

`pg_settings` is a view that allows to inspect all the available settings in the Postgres instance,
across all contexts -- from internal to user.



```
# select * from pg_settings where name = 'work_mem';

-[ RECORD 1 ]---+----------------------------------------------------------------------------------------------------------------------
name            | work_mem
setting         | 5051
unit            | kB
category        | Resource Usage / Memory
short_desc      | Sets the maximum memory to be used for query workspaces.
extra_desc      | This much memory can be used by each internal sort operation and hash table before switching to temporary disk files.
context         | user
vartype         | integer
source          | configuration file
min_val         | 64
max_val         | 2147483647
enumvals        | 
boot_val        | 4096
reset_val       | 5051
sourcefile      | /etc/postgresql/11/main/postgresql.conf
sourceline      | 126
pending_restart | f
```

## Change that setting locally by using SET and check the results

```
template1=# SET work_mem to '1MB';
SET
template1=# select * from pg_settings where name='work_mem';
-[ RECORD 1 ]---+----------------------------------------------------------------------------------------------------------------------
name            | work_mem
setting         | 1024
unit            | kB
category        | Resource Usage / Memory
short_desc      | Sets the maximum memory to be used for query workspaces.
extra_desc      | This much memory can be used by each internal sort operation and hash table before switching to temporary disk files.
context         | user
vartype         | integer
source          | session
min_val         | 64
max_val         | 2147483647
enumvals        | 
boot_val        | 4096
reset_val       | 5051
sourcefile      | 
sourceline      | 
pending_restart | f
```


### Reset the value back to the original

```
template1=# reset work_mem ;
RESET
template1=# select * from pg_settings where name='work_mem';
-[ RECORD 1 ]---+----------------------------------------------------------------------------------------------------------------------
name            | work_mem
setting         | 5051
unit            | kB
category        | Resource Usage / Memory
short_desc      | Sets the maximum memory to be used for query workspaces.
extra_desc      | This much memory can be used by each internal sort operation and hash table before switching to temporary disk files.
context         | user
vartype         | integer
source          | configuration file
min_val         | 64
max_val         | 2147483647
enumvals        | 
boot_val        | 4096
reset_val       | 5051
sourcefile      | /etc/postgresql/11/main/postgresql.conf
sourceline      | 126
pending_restart | f
```