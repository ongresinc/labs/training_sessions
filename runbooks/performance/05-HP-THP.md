

Disabling THP

```
echo never > /sys/kernel/mm/transparent_hugepage/enabled
echo never > /sys/kernel/mm/transparent_hugepage/defrag
```

HP:

```
root@ubuntu-xenial:~# pmap $(pgrep -o postgres) | awk '/rw-s/ && /zero/ {print $2}'
145616K

show shared_buffers;
(128*1024)/2048
select context from pg_settings where name = 'huge_pages';

sysctl -w vm.nr_hugepages=71
```