### Easy way to measure checkpoint activity

1) Simulate write activity, using pgbench or similar
2) Use pg_current_wal_insert_lsn() to get current wal position

```
postgres=# select pg_current_wal_insert_lsn();  
---------------------------
 0/E67EA500
 (1 row)
```

Wait 1 minute or so, and repeat 

select pg_sleep(60); select pg_current_wal_insert_lsn();
```
 pg_current_wal_insert_lsn 
---------------------------
 1/35955CF0
(1 row)
```

Then, substract both (printing it nicely):
```
postgres=# select pg_size_pretty(pg_wal_lsn_diff('1/35955CF0', '0/E67EA500'));
 pg_size_pretty 
----------------
 1265 MB
(1 row)
```

That is ~ 1G / minute. Remember that, due to spread checkpoints, _max_wal_size_ quota can be divided into 2-3 checkpoints.

So, if you are targeting _checkpoint_timeout_ = 30 minutes you could use the formula

```
_max_wal_size_ : WAL activity (during 30 minutes) * 3
```
```
=> _max_wal_size_ ~ 1G * 30 (minutes) * 3
```

```
=> _max_wal_size_ ~ 90G
```