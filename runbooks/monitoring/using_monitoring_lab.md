## Monitoring lab

```
cd docker/monitoring
docker-compose up -d
```

Generate a decent amount of data:

```
PGPASSWORD=postgres pgbench -h localhost -p 15555 -U postgres -i -s100
```

Access the grafana:

At toninas: http://toninas.mooo.con:13080
If running locally: http://localhost:13080


Generate traffic through PgBouncer:

```
PGPASSWORD=postgres pgbench -h localhost -p 15555 -U postgres -c 100 -T1000
```

You can also run activity to Postgres directly:

```
PGPASSWORD=postgres pgbench -h localhost -p 15566 -U postgres -c 80 -T1000
```