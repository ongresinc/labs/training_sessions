1. Execute on database:

```sql
ALTER SYSTEM SET shared_preload_libraries TO auto_explain;
ALTER SYSTEM SET auto_explain.log_buffers TO true;
ALTER SYSTEM SET auto_explain.log_min_duration TO 0;
```

2. Restart postgres service:

```bash
sudo pg_ctlcluster 13 main restart
```

3. And check the logs!

```bash
tail -f /var/log/postgresql/postgresql-13.log
```
