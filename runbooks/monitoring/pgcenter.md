# PgCenter Runbook

Use `docker/pgcenter`. 

## Populate the testing database

First, execute the database load duirng 10 min:

```bash
docker-compose exec pg1 /bin/bash -c  "pgbench -n -c 20 -T 600 -d -f /scripts/custom_queries.sql  -U postgres dell"
```


## pgcenter

In another terminal while(10 min) run the load 

```bash
docker-compose exec pgcenter pgcenter top -h pg1 -U postgres -d dell
```

### record stats

```bash
docker-compose exec pgcenter pgcenter record -i 10s -c 20 -f /outputs/record_stats.tar -h pg1 -U postgres -d dell
```

### Report

```bash

docker-compose exec pgcenter pgcenter report -f /outputs/record_stats.tar --activity
docker-compose exec pgcenter pgcenter report -f /outputs/record_stats.tar --tables
docker-compose exec pgcenter pgcenter report -f /outputs/record_stats.tar --indexes
docker-compose exec pgcenter pgcenter report -f /outputs/record_stats.tar -D g
docker-compose exec pgcenter pgcenter report -f /outputs/record_stats.tar -A --start  17:48:00 --end 17:48:10
```

 

### Stop the container
```
sh clean.sh
```

