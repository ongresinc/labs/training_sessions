### Verify Postgres cache is being used. 

Numbers for cache_effectivity below 0.8 usually means that you could raise your `shared_buffers` setting.


Show cache usage globally:
```sql
select datname, round((blks_hit::float / (blks_hit + blks_read))::numeric * 100, 2) as cache_effectivity 
from pg_stat_database
where (blks_hit + blks_read)>0 and datid>0;
```

Sample output:
```
       datname       | cache_effectivity  
---------------------+--------------------
 production          | 0.9738560600578243
 postgres            | 0.997014701839577
```

If you want to inspect cache effectivity at table level:
```sql
select relname, pg_size_pretty(pg_relation_size(schemaname || '.' || relname)),
case when (heap_blks_hit + heap_blks_read) > 0 then round((heap_blks_hit::float / (heap_blks_hit + heap_blks_read)) * 100) else null end as heap_cache_eff,
case when (idx_blks_hit + idx_blks_read) > 0 then round(idx_blks_hit::float / (idx_blks_hit + idx_blks_read) * 100) else null end  as idx_cache_eff,
case when (toast_blks_hit + toast_blks_read) > 0 then round(toast_blks_hit::float / (toast_blks_hit + toast_blks_read) * 100) else null end  as toast_cache_eff,
case when (tidx_blks_hit + tidx_blks_read) > 0 then round(tidx_blks_hit::float / (tidx_blks_hit + tidx_blks_read) * 100) else null end as tidx_cache_eff
from pg_statio_user_tables
where pg_relation_size(schemaname || '.' || relname) > 1000000 -- show only "big tables here"

order by heap_cache_eff desc
;
```