### Get the oldest running queries from this database

select pid, query_start,wait_event_type,wait_event,query, now() - query_start as age
from pg_stat_activity where state = 'active' -- show only actives backends
and now() - query_start > interval '1 second' -- Choose wich "old" fits better for your workload
order by age desc;

Sample output:

```
-[ RECORD 1 ]---+--------------------------------------------
pid             | 63912
query_start     | 2021-05-26 19:00:16.460448+00
wait_event_type | 
wait_event      | 
query           | autovacuum: VACUUM public.table
age             | 00:06:55.023772
```

TIP: you could add a filter
``` 
.... and backend_type = 'client backend'
```

in order to show only queries from "normal" backends