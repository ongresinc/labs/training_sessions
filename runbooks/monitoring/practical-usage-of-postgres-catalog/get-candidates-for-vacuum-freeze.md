### Get candidates for VACUUM FREEZE

Autovacuum (to avoid wraparound) is an aggresive way of vacuum, it needs to read the entire table, so it consumes lot of IO power.
This query allows us to monitor when this situation is about to happen (specially for the biggest tables), so you can plan a `VACUUM FREEZE` 
in a moment of lower workload.

```sql
WITH per_table_stats AS (
  SELECT
    c.oid::regclass AS relation,
    age(c.relfrozenxid) AS oldest_current_xid,
    current_setting('autovacuum_freeze_max_age')::float AS autovacuum_freeze_max_age,
    pg_size_pretty(pg_total_relation_size(c.oid)) AS size
  FROM
    pg_class c
    JOIN pg_namespace n on c.relnamespace = n.oid
  WHERE
    relkind IN ('r', 't', 'm') 
    AND n.nspname NOT IN ('pg_toast')
  ORDER BY 
    oldest_current_xid DESC
)
SELECT
  relation,
  oldest_current_xid,
  autovacuum_freeze_max_age,
  round(100*(oldest_current_xid/autovacuum_freeze_max_age::float)) AS percent_towards_emergency_autovacuum,
  size
FROM
  per_table_stats
  where pg_total_relation_size(relation) > 1000000 -- filtering by table size, as vacuuming small tables will not impact your system
LIMIT
  10;
```

Sample output
```
                   relation         | oldest_current_xid | autovacuum_freeze_max_age | percent_towards_emergency_autovacuum |    size    
------------------------------------+--------------------+---------------------------+--------------------------------------+------------
 table1                             |          185106698 |                 200000000 |                                   99 | 32 MB
 table2                             |          185106698 |                 200000000 |                                   99 | 56 kB

```