## Tables with high UPDATE ratio are good candidates for applying FILLFACTOR
## Remember, default FILLFACTOR is 100. Information about FILLFACTOR is tracked in `pg_class.reloptions`

```sql
select schemaname || '.' || ut.relname as tablename, 
n_tup_upd::float / (n_tup_upd + n_tup_ins + n_tup_del) as upd_ratio, 
pg_size_pretty(pg_relation_size(schemaname || '.' || ut.relname)),
c.reloptions 
from pg_stat_user_tables ut join pg_class c on (ut.relname = c.relname)

where (n_tup_upd + n_tup_ins + n_tup_del) > 0 
and pg_relation_size(schemaname || '.' || ut.relname) > 1000000 -- only show "big" tables here
order by 2 desc limit 20;
```

Sample output:
```
                 ?column?                 |      ?column?      | pg_size_pretty |                         reloptions                         
------------------------------------------+--------------------+----------------+------------------------------------------------------------
 public.projects                          | 0.9996531893815684 | 1339 MB        | {autovacuum_vacuum_cost_limit=10000,autovacuum_enabled=on}
 public.project_features                  | 0.9993889771831674 | 36 MB          | {autovacuum_enabled=on}
 public.users                             | 0.9917855729960632 | 4159 MB        | {autovacuum_enabled=on}
 
```