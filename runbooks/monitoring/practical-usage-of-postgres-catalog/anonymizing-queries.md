## Anonymizing queries

Using regexp


Removing whats inside quotes:

```sql
SELECT regexp_replace($$SELECT * from table where id = 4 and username = 'john'$$  , E'''[^'']+''', '?', 'g');

                   regexp_replace                   
----------------------------------------------------
  SELECT * from table where id = 4 and username = ?

```
Removing numeric filters

```sql
SELECT regexp_replace($$ SELECT * from table where id = 4 and username='john'$$,'[0-9]+','?','g'); 
                    regexp_replace                     
-------------------------------------------------------
  SELECT * from table where id = ? and username='john'
(1 row)
```




Removing comments
```
template1=# select regexp_replace($_$/* some comment */ SELECT * from mytable  $_$, '\/\*.*\*\/', '');
      regexp_replace      
--------------------------
  SELECT * from mytable  
(1 row)
```


Putting all together

```
select substr(query,1,200), count(*) from
(
select regexp_replace( 
    regexp_replace( regexp_replace(query,'[-]?[0-9]+','?','g')  , E'''[^'']+''', '?', 'g')
    , '\/\*.*\*\/', '') query
     from pg_stat_activity where pid != pg_backend_pid()
) foo
group by 1 order by 2 desc
```
