> Use the `docker/monitoring` lab.

```
docker-compose up -d
```

The below initiates the database:

```
PGPASSWORD=postgres pgbench -h localhost -p 15555 -U postgres -i -s40
```

Open grafana at localhost:13080 or point to toninas.mooo.com:13080.

Try to execute concurrent 500 connections to Postgres directly, you'll see
errors on connections:

```
PGPASSWORD=postgres pgbench -h localhost -p 15566 -U postgres -c500 -C -T1000
```

Now, execute the same on but against pgbouncer:

```
PGPASSWORD=postgres pgbench -h localhost -p 15555 -U postgres -c500 -C -T1000
```

- max_connections is 100, although we are capable to manage more than 100 connections
  thanks to max_client_conn in the PgBouncer.
- pool_size is 4, which is low, but safe.



Tweak `pgbouncer/pgbouncer.ini` and reload pgbouncer to show the impact on pool_size:

```
docker-compose exec pgbouncer sh -c "kill -HUP \$(pgrep pgbouncer)"
# or restart
docker-compose restart pgbouncer   
```

Compare efficiency between Postgres and Pgbouncer:

```
# This is for a transaction mode
PGPASSWORD=postgres pgbench -h localhost -p 15555 -U postgres -c80 -C -T60
PGPASSWORD=postgres pgbench -h localhost -p 15566 -U postgres -c80 -C -T60
```

```
# This is as session mode
PGPASSWORD=postgres pgbench -h localhost -p 15555 -U postgres -c80  
PGPASSWORD=postgres pgbench -h localhost -p 15566 -U postgres -c80 
```

```
# Trying to issue more persistent connections than Postgres can handle.
# pgbouncer ok:
PGPASSWORD=postgres pgbench -h localhost -p 15555 -U postgres -c200  
# postgres won't be able:
PGPASSWORD=postgres pgbench -h localhost -p 15566 -U postgres -c200 
```

You will see that connecting to Postgres is always faster when using
low amount of connections, there is an overhead. Although, without
PgBouncer, Postgres won't be able to handle a large set of connections.

Although, using -C (connections are re-issued), the delta compared
with pgbouncer is lower.




To clean:

```
docker-compose down
sudo rm -rf .data
```