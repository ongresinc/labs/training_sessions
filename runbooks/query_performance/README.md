## Exercises for Lab SQL optimizations in PG 12

The exercises :

 1. Use pgbadger tool to analyze the PostgreSQL's logs locate in `/var/lib/postgresql/data/log`. Please choose two queries from this report and optimize them.

 2. Get the top 5 queries with more `mean_time` from `pg_stat_statements` view


 3. ANALYZE AND OPTIMIZE THE FOLLOWING QUERIES

    Please use the online free tools for analyze the execution plan
(​https://explain.depesz.com/ or ​https://explain.dalibo.com/​), also consider use: parameters changes,  indexes, query rewrite, etc
    
 ```sql
  SELECT * FROM customers order by 2;
 ```
 
```sql 
SELECT firstname, lastname, orderdate, totalamount, tax,netamount 
    FROM customers join orders using (customerid) 
    WHERE tax<10 and age=20 order by orderdate ;
```

```sql 
SELECT actor,price FROM products where special=1 and price<10 ;
```

```sql
 SELECT * FROM texts where t like '%postgresql%' order by i;
```

```sql 
SELECT 
(select count(*) as "M" 
    FROM products  
    WHERE price <20 ) as under_20, (select count(*) as "F" from products where price >=20  ) as over_20,(select count(*) as "M" from products ) as  TOTAL;
```
   
     


