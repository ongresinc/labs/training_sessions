Lab docker/patroni_w_haproxy implements the same as envoy, but using static ips.


```
listen postgres
    bind *:5432
    option httpchk
    http-check expect status 200
    default-server inter 3s fall 3 rise 2 on-marked-down shutdown-sessions
    server pg1 172.16.234.21:5432 maxconn 100 check port 8008
    server pg2 172.16.234.22:5432 maxconn 100 check port 8008
```

We access through forwarded port:

```
PGPASSWORD=postgres psql -h localhost -p 15432 -Upostgres postgres
```

Through RO:

```
PGPASSWORD=postgres psql -h localhost -p 15444 -Upostgres postgres
```

```
echo "show servers state servers" | nc localhost 9000
```

> Haproxy does not support resolving DNS via TCP. When the response size crosses accepted_payload_size you will get that behavior.

I couldn't add a round robin RO pool using TCP and DNS resolver :disappointed:
https://discourse.haproxy.org/t/server-template-limitations-is-tcp-supported/6185

