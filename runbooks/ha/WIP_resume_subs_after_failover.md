
Can a subscription be attached to a Publication after a server has been promoted?

```
CREATE SUBSCRIPTION cross_version_pg2_subs CONNECTION 'host=10.0.0.12 user=postgres password=postgres dbname=demo2' PUBLICATION cross_version_pg1_pub WITH (copy_data = true, enabled = true);

ALTER SUBSCRIPTION cross_version_pg2_subs CONNECTION 'host=10.0.0.11 port=5433 user=postgres password=postgres dbname=demo2';
ALTER SUBSCRIPTION cross_version_pg2_subs ENABLE;
ALTER SUBSCRIPTION cross_version_pg2_subs REFRESH PUBLICATION;
```

Although, this does not work, need to double check this.