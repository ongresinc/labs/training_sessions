
- Use docker/logical-replication

- Run

```
sudo ./init.sh
./demo.sh
```

demo script runs a simple LR between 13 and 11 versions.

Container does not have `ps` utility as is a tight version, do in pg1 and pg2:

```
docker-compose exec pg1 bash
apt-get update && apt-get install -y procps
```

You can access the services:

```
docker-compose exec pg1 psql
```

Or execute queries:

```
docker-compose exec pg1 psql -Upostgres -c "SELECT * from cross_version" demo
```

Or connecting to exposed port:

```
PGPASSWORD=postgres psql -q -h localhost -p 15555 -U postgres demo
```


## Monitoring

```
docker-compose exec pg1 psql -Upostgres

postgres=# select * from pg_replication_slots ;
-[ RECORD 1 ]-------+-----------------------
slot_name           | cross_version_pg2_subs
plugin              | pgoutput
slot_type           | logical
datoid              | 16398
database            | demo
temporary           | f
active              | t
active_pid          | 549
xmin                | 
catalog_xmin        | 504
restart_lsn         | 0/160E520
confirmed_flush_lsn | 0/160E558
wal_status          | reserved
safe_wal_size       | 
```

```
docker-compose exec pg2 psql -Upostgres


postgres=# select * from pg_replication_origin_status ;
 local_id | external_id | remote_lsn | local_lsn 
----------+-------------+------------+-----------
        1 | pg_16409    | 0/160E520  | 0/168A368
(1 row)

postgres=# select * from pg_replication_origin ;
 roident |  roname  
---------+----------
       1 | pg_16409
(1 row)
```