
Consul maintenance mode (for external nodes to Patroni):

```
docker-compose exec consul-agent1 sh


consul maint -enable
consul maint -disable
```

```
consul lock -timeout 2s -child-exit-code $LOG

LOCKNAME="aprocess"
RESOURCE="$LOCKNAME"
MAX_AGE='108000' # 30 Hours.
PROM_SHARD='shard/default'
PROM_TIER='tier/db'
PROM_TYPE='type/patroni'

PUSH_URL="http://prometheus:9090/metrics/job/$LOCKNAME/${PROM_SHARD}/${PROM_TIER}/${PROM_TYPE}"


LOCKNAME="aprocess"
consul lock -timeout 1s -child-exit-code $LOCKNAME "sleep 10"


```
consul kv export
```
