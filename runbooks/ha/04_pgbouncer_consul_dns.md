Integrated using resolv_conf at the pgbouncer.ini, although you may want to make Consul DNS integrated
with your existing DNS servers.

```
PGPASSWORD=postgres psql -h localhost -p 16432 -Upostgres postgres
```

Admin interface:

```
PGPASSWORD=postgres psql -h localhost -p 16432 -Upostgres pgbouncer
```