

## Restoring in a different directory

Useful for restoring for test purposes:

```
-bash-4.2$ mkdir /tmp/datatest
-bash-4.2$ pgbackrest --stanza=db-server-1 --pg1-path=/tmp/datatest restore 
-bash-4.2$ echo -e "\nport=5555" >> /tmp/datatest/postgresql.conf
-bash-4.2$ /usr/pgsql-12/bin/pg_ctl -D /tmp/datatest/ start 
-bash-4.2$ ps f -Cpostgres
  PID TTY      STAT   TIME COMMAND
29484 ?        Ss     0:00 /usr/pgsql-12/bin/postgres -D /tmp/datatest
29490 ?        Ss     0:00  \_ postgres: checkpointer   
29491 ?        Rs     0:00  \_ postgres: background writer   
29493 ?        Ss     0:00  \_ postgres: stats collector   
29501 ?        Rs     0:00  \_ postgres: walwriter   
29502 ?        Ss     0:00  \_ postgres: autovacuum launcher   
29503 ?        Ss     0:00  \_ postgres: archiver   failed on 00000002.history
29504 ?        Ss     0:00  \_ postgres: logical replication launcher   
```

To setup as a replica:

> Note: Add the line in the pg_hba.conf!

```
echo -e "host    replication    all       127.0.0.1/32   md5\n" >> /var/lib/pgsql/12/data/pg_hba.conf
```

```
primary_conninfo = 'host=127.0.0.1 port=5432 password=postgres user=postgres sslmode=prefer'
```


## Manual Setup of Replication

2 terms:

```
vagrant ssh db-server-1
vagrant ssh db-server-2
```

> 10.0.0.11
> 10.0.0.12


## Replicating from pg12



On the leader:

```
echo -e "host   replication all 10.0.0.0/24 md5\n" >> /var/lib/pgsql/12/data/pg_hba.conf
echo -e "host    replication    all       127.0.0.1/32   md5\n" >> /var/lib/pgsql/12/data/pg_hba.conf
sudo systemctl reload postgresql-12.service
```

```
SELECT pg_create_physical_replication_slot('demo');
```

On the slave:

```
PGPASSWORD=postgres pg_basebackup -h 10.0.0.12 -Upostgres -D /tmp/replica-12 --checkpoint=fast -P --write-recovery-conf 
```


```
sudo chown -R postgres: /tmp/replica-12/

sudo su - postgres
echo -e "port=5433\nprimary_slot_name = 'demo'\n" >> /tmp/replica-12/postgresql.auto.conf
/usr/pgsql-12/bin/pg_ctl -D /tmp/replica-12/ start
```

On the leader, check slots:
```
select * from pg_replication_slots ;
```

Clean up

```
/usr/pgsql-12/bin/pg_ctl -D /tmp/replica-12/ stop
```


Reference: [Runtime replication config](https://www.postgresql.org/docs/12/runtime-config-replication.html)