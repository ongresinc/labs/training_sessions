

Get the nodes in the cluster and change accordingly:

```
docker-compose exec pg1 patronictl list 
docker-compose exec pg1 patronictl switchover --master f35536855f35 --candidate e0492de9ee7b --force
```

Failover:

```
 docker-compose exec pg1 patronictl failover --master e0492de9ee7b --candidate f35536855f35 --force
Current cluster topology
+ Cluster: testcluster (6965649442097979423) -------+----+-----------+
| Member       | Host           | Role    | State   | TL | Lag in MB |
+--------------+----------------+---------+---------+----+-----------+
| e0492de9ee7b | 192.168.224.11 | Leader  | running |  2 |           |
| f35536855f35 | 192.168.224.10 | Replica | running |  2 |         0 |
+--------------+----------------+---------+---------+----+-----------+
2021-05-24 00:46:08.99766 Successfully switched over to "f35536855f35"
+ Cluster: testcluster (6965649442097979423) -------+----+-----------+
| Member       | Host           | Role    | State   | TL | Lag in MB |
+--------------+----------------+---------+---------+----+-----------+
| e0492de9ee7b | 192.168.224.11 | Replica | stopped |    |   unknown |
| f35536855f35 | 192.168.224.10 | Leader  | running |  2 |           |
+--------------+----------------+---------+---------+----+-----------+
```


http://192.168.0.176:18001/clusters

```
postgres_cluster::172.16.236.9:5432::health_flags::/failed_active_hc
postgres_cluster::172.16.236.2:5432::health_flags::healthy
```

