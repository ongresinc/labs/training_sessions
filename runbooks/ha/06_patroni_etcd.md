# etcd / Patroni specifics


## etcd quick sheet

Get keys:

```sh
$ etcdctl get / --prefix --keys-only

/service/testcluster/config
/service/testcluster/initialize
/service/testcluster/leader
/service/testcluster/members/3775fff2adf1
/service/testcluster/members/e545f6f75db8
/service/testcluster/status
```

```sh
root@68432f19b776:/# etcdctl get /service/testcluster/members/e545f6f75db8       
/service/testcluster/members/e545f6f75db8
```

```json
{"conn_url":"postgres://172.16.220.7:5432/postgres",
 "api_url":"http://172.16.220.7:8008/patroni",
 "state":"running","role":"replica","version":"2.1.2","xlog_location":67108960,"timeline":1}
```

```sh
root@68432f19b776:/# etcdctl get /service/testcluster/initialize
/service/testcluster/initialize
7060473567077326880
```

```
postgres@e545f6f75db8:/$ pg_controldata data/testcluster
pg_control version number:            1201
Catalog version number:               201909212
Database system identifier:           7060473567077326880
```

Patroni configuration:

```sh
root@68432f19b776:/$ etcdctl get /service/testcluster/config
/service/testcluster/config
```

```json
{"ttl":90,"loop_wait":10,"retry_timeout":40,"maximum_lag_on_failover":1048576,"postgresql":{"use_pg_rewind":true,"use_slots":true,"parameters":{"wal_level":"logical","hot_standby":"on","wal_keep_segments":0,"max_wal_senders":6,"max_replication_slots":6,"checkpoint_timeout":"10min","max_connections":100,"max_wal_size":"1GB"}}}
```

Get all keys and values:

```sh
etcdctl get / --prefix=true
```




[CLI sheet](https://lzone.de/cheat-sheet/etcd)