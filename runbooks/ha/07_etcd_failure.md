
```
docker-compose exec pg patronictl list
+--------------+--------------+---------+---------+----+-----------+
| Member       | Host         | Role    | State   | TL | Lag in MB |
+ Cluster: testcluster (7060877393500033056) -----+----+-----------+
| 1850d68558e0 | 172.16.100.5 | Replica | running |  1 |         0 |
| e2035bbcac42 | 172.16.100.7 | Leader  | running |  1 |           |
+--------------+--------------+---------+---------+----+-----------+
```



```
docker-compose exec etcd2 etcdctl member list
88a3987c1f3a0b42, started, etcd2, http://etcd2:2380, http://etcd2:2379, false
b235e980b6200a72, started, etcd1, http://etcd1:2380, http://etcd1:2379, false
c481b5bee1a2c478, started, etcd3, http://etcd3:2380, http://etcd3:2379, false
```

```
patroni_etcd [main●●] docker-compose exec etcd2 etcdctl --write-out=table --endpoints=etcd1:2380,etcd2:2380,etcd3:2380 endpoint status
+------------+------------------+---------+---------+-----------+------------+-----------+------------+--------------------+--------+
|  ENDPOINT  |        ID        | VERSION | DB SIZE | IS LEADER | IS LEARNER | RAFT TERM | RAFT INDEX | RAFT APPLIED INDEX | ERRORS |
+------------+------------------+---------+---------+-----------+------------+-----------+------------+--------------------+--------+
| etcd1:2380 | b235e980b6200a72 |  3.4.15 |   25 kB |      true |      false |         2 |         31 |                 31 |        |
| etcd2:2380 | 88a3987c1f3a0b42 |  3.4.15 |   25 kB |     false |      false |         2 |         31 |                 31 |        |
| etcd3:2380 | c481b5bee1a2c478 |  3.4.15 |   25 kB |     false |      false |         2 |         31 |                 31 |        |
+------------+------------------+---------+---------+-----------+------------+-----------+------------+--------------------+--------+
```

```
patroni_etcd [main●●] docker-compose exec etcd2 etcdctl --write-out=table --endpoints=etcd1:2380,etcd2:2380,etcd3:2380 endpoint health
+------------+--------+------------+-------+
|  ENDPOINT  | HEALTH |    TOOK    | ERROR |
+------------+--------+------------+-------+
| etcd2:2380 |   true | 3.306055ms |       |
| etcd3:2380 |   true | 5.296704ms |       |
| etcd1:2380 |   true |  6.08086ms |       |
+------------+--------+------------+-------+
```

Stop the leader:

```
docker-compose stop etcd1
```

```
patroni_etcd [main●●] docker-compose exec etcd2 etcdctl --write-out=table --endpoints=etcd1:2380,etcd2:2380,etcd3:2380 endpoint health

{"level":"warn","ts":"2022-02-04T15:34:05.257Z","caller":"clientv3/retry_interceptor.go:62","msg":"retrying of unary invoker failed","target":"endpoint://client-7d8cba6b-9bbd-4507-8c31-15b5e249a030/etcd1:2380","attempt":0,"error":"rpc error: code = DeadlineExceeded desc = latest balancer error: all SubConns are in TransientFailure, latest connection error: connection error: desc = \"transport: Error while dialing dial tcp: lookup etcd1 on 127.0.0.11:53: server misbehaving\""}
+------------+--------+--------------+---------------------------+
|  ENDPOINT  | HEALTH |     TOOK     |           ERROR           |
+------------+--------+--------------+---------------------------+
| etcd3:2380 |   true |   4.017799ms |                           |
| etcd2:2380 |   true |   4.141777ms |                           |
| etcd1:2380 |  false | 5.000795523s | context deadline exceeded |
+------------+--------+--------------+---------------------------+
Error: unhealthy cluster
```

```
patroni_etcd [main●●] docker-compose exec etcd2 etcdctl --write-out=table --endpoints=etcd1:2380,etcd2:2380,etcd3:2380 endpoint status
{"level":"warn","ts":"2022-02-04T15:34:35.939Z","caller":"clientv3/retry_interceptor.go:62","msg":"retrying of unary invoker failed","target":"passthrough:///etcd1:2380","attempt":0,"error":"rpc error: code = DeadlineExceeded desc = latest balancer error: connection error: desc = \"transport: Error while dialing dial tcp: lookup etcd1 on 127.0.0.11:53: server misbehaving\""}
Failed to get the status of endpoint etcd1:2380 (context deadline exceeded)
+------------+------------------+---------+---------+-----------+------------+-----------+------------+--------------------+--------+
|  ENDPOINT  |        ID        | VERSION | DB SIZE | IS LEADER | IS LEARNER | RAFT TERM | RAFT INDEX | RAFT APPLIED INDEX | ERRORS |
+------------+------------------+---------+---------+-----------+------------+-----------+------------+--------------------+--------+
| etcd2:2380 | 88a3987c1f3a0b42 |  3.4.15 |   25 kB |     false |      false |         3 |         32 |                 32 |        |
| etcd3:2380 | c481b5bee1a2c478 |  3.4.15 |   25 kB |      true |      false |         3 |         32 |                 32 |        |
+------------+------------------+---------+---------+-----------+------------+-----------+------------+--------------------+--------+
```

Stop the new leaderleader:

```
docker-compose stop etcd3
```

```
patroni_etcd [main●●] docker-compose exec etcd2 etcdctl --write-out=table --endpoints=etcd1:2380,etcd2:2380,etcd3:2380 endpoint status
{"level":"warn","ts":"2022-02-04T15:36:01.683Z","caller":"clientv3/retry_interceptor.go:62","msg":"retrying of unary invoker failed","target":"passthrough:///etcd1:2380","attempt":0,"error":"rpc error: code = DeadlineExceeded desc = context deadline exceeded"}
Failed to get the status of endpoint etcd1:2380 (context deadline exceeded)
{"level":"warn","ts":"2022-02-04T15:36:06.689Z","caller":"clientv3/retry_interceptor.go:62","msg":"retrying of unary invoker failed","target":"passthrough:///etcd3:2380","attempt":0,"error":"rpc error: code = DeadlineExceeded desc = latest balancer error: connection error: desc = \"transport: Error while dialing dial tcp: lookup etcd3 on 127.0.0.11:53: server misbehaving\""}
Failed to get the status of endpoint etcd3:2380 (context deadline exceeded)
+------------+------------------+---------+---------+-----------+------------+-----------+------------+--------------------+-----------------------+
|  ENDPOINT  |        ID        | VERSION | DB SIZE | IS LEADER | IS LEARNER | RAFT TERM | RAFT INDEX | RAFT APPLIED INDEX |        ERRORS         |
+------------+------------------+---------+---------+-----------+------------+-----------+------------+--------------------+-----------------------+
| etcd2:2380 | 88a3987c1f3a0b42 |  3.4.15 |   25 kB |     false |      false |         4 |         33 |                 33 | etcdserver: no leader |
+------------+------------------+---------+---------+-----------+------------+-----------+------------+--------------------+-----------------------+
```