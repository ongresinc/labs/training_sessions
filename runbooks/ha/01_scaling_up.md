
Use docker/patroni (lightweight)

```
docker-compose build

docker-compose up -d --scale pg=3

```

```
docker-compose exec pg patronictl list

+ Cluster: testcluster (6965226074778251295) --+----+-----------+
| Member       | Host       | Role   | State   | TL | Lag in MB |
+--------------+------------+--------+---------+----+-----------+
| 35f4feeb0378 | 172.19.0.4 | Leader | running |  1 |           |
+--------------+------------+--------+---------+----+-----------+
```


```

docker-compose exec pg patronictl list

+ Cluster: testcluster (6965226074778251295) ---+----+-----------+
| Member       | Host       | Role    | State   | TL | Lag in MB |
+--------------+------------+---------+---------+----+-----------+
| 35f4feeb0378 | 172.19.0.4 | Leader  | running |  1 |           |
| d648e6ae5d67 | 172.19.0.6 | Replica | running |  1 |         0 |
| e37c9d9bacd1 | 172.19.0.7 | Replica | running |  1 |        16 |
```

Access a container in a service by using `--index`

```
 docker-compose exec --index=3 pg bash
```

Or, you can access as root by:

```
docker exec -it  -u 0 patroni_pg_3 bash
```

```
patronictl topology
patronictl show-config
```

Do a switchover with no confirmation from any box that is not the leader:

```
patronictl switchover testcluster --master=35f4feeb0378 --candidate=d648e6ae5d67 --force
```


Scaling down:

```
docker-compose up -d --scale pg=2
watch docker-compose exec pg patronictl topology 
```


Access through pgbouncer with DNS integration:

```
PGPASSWORD=postgres psql -h localhost -p 16432 -Upostgres postgres
```

