# pgbackrest

## setup

Install vagrant and virtualbox:

```bash
brew install virtualbox vagrant ansible
```

Linux

```bash
sudo apt-get install linux-headers-$(uname -r) build-essential dkms vagrant ansible virtualbox virtualbox-dkms
```

[Vagrant VB provider](https://www.vagrantup.com/docs/providers/virtualbox/boxes)

## how it works

This lab contains details about how to setup pgbackrest on your local machine.

the `Makefile` contains all steps necessary. It basically does:

* `up` to create the machines
* `down` to shutdown the machines
* `clean` to remove the machines and clean up the `~/.know_hosts` file
* `provision` to execute the `postgres` and `pgbackrest` playbooks that will install:
    * 2 postgres servers (`10.0.0.11` and `10.0.0.20`) with `PostgreSQL 12.2`
    * 1 backup server `10.0.0.20` with Pgbackrest `2.24`
> Check the `inventory.yml` file for details.


## fast setup

```
python3 -m venv .venv 
source .venv/bin/activate
pip install -r requirements.txt
```

To create the servers and setup them, just type:

```bash
make up provision
```

## Post cleanup

If you get the following error:

> WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!

Remove the previous entries in the known_hosts:

```
ssh-keygen -f ~/.ssh/known_hosts -R "10.0.0.11"
ssh-keygen -f ~/.ssh/known_hosts -R "10.0.0.12"
```